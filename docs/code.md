---
author: Votre nom
title: code
---

!!! info "écrire une fonction somme"

La variable `nombres` est de type `#!py list` et la valeur référencée est `#!py [1, 2, 3]`
 {{ IDE() }}

On peut avoir aussi un IDE vertical.

{{ terminal() }}

Compléter le script ci-dessous.
N'oubliez pas de valider les tests après avoir exécuté.    

{{ IDE('scripts/somme', MAX=3, SANS='sum' )}}

