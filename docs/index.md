---
titre : Accueil
hide :
    -toc
---


# Bienvenue sur le site du lycée Honfleur Albert Sorel Terminale NSI

Ceci est la page d'accueil du site.

# forge.apps.education.fr

![Logo Forge](images/LogoLaForge.svg){ width=50% }

Ce site est le document d'accompagnement de la formation "Forge des Communs Numériques Éducatifs" 

## Sommaire


